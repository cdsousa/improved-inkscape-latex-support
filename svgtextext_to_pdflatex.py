#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""\
Convert SVG that includes TexText objects to PDF + LaTeX.

Author: Håkon Marthinsen
"""

# Copyright (C) 2012 Håkon Marthinsen
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

__version__ = '0.1.0'
__author__ = 'Håkon Marthinsen'

import inkex
import os
import sys
from textext import TexText, exec_command
from tempfile import mkstemp

import pygtk
pygtk.require('2.0')

import gtk

# Check for new pygtk: this is new class in PyGtk 2.4
if gtk.pygtk_version < (2, 3, 90):
    inkex.debug("PyGtk 2.3.90 or later required")
    raise SystemExit

TEXTEXT_NS = u"http://www.iki.fi/pav/software/textext/"
SVG_NS = u"http://www.w3.org/2000/svg"
SODIPODI_NS = u"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"


class TexTextExporter(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)

        # Choose an output folder.
        dialog = gtk.FileChooserDialog(
            title='Save as a PDF document',
            action=gtk.FILE_CHOOSER_ACTION_SAVE,
            buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                     gtk.STOCK_OPEN, gtk.RESPONSE_OK))

        dialog.set_default_response(gtk.RESPONSE_OK)

        filefilter = gtk.FileFilter()
        filefilter.set_name("PDF document")
        filefilter.add_mime_type("application/pdf")
        filefilter.add_pattern("*.pdf")
        dialog.add_filter(filefilter)

        filefilter = gtk.FileFilter()
        filefilter.set_name("All files")
        filefilter.add_pattern("*")
        dialog.add_filter(filefilter)

        response = dialog.run()

        if response == gtk.RESPONSE_OK:
            self.outputpdf = dialog.get_filename()
        elif response == gtk.RESPONSE_CANCEL:
            raise SystemExit
        dialog.destroy()

    def effect(self):
        svg_file = sys.argv[-1]
        stream = open(svg_file, 'r')
        document = inkex.etree.parse(stream)
        stream.close()

        nodes = document.getroot()

        # Create list of TexText nodes.
        textextnodes = []
        for node in nodes.iter('{%s}g' % SVG_NS):
            if '{%s}text' % TEXTEXT_NS in node.attrib:
                textextnodes.append(node)

        for node in textextnodes:
            text = node.attrib['{%s}text'%TEXTEXT_NS].decode('string-escape')
            transform = node.attrib['transform']
            style = node.attrib['style'] if 'style' in node.attrib else None
            node.clear()
            node.tag = 'text'
            node.attrib['transform']= transform
            if style:
                node.attrib['style']= style
            node.text = text

        # Write document to temporary file.
        cwd = os.getcwd()

        outputfolder = os.path.dirname(self.outputpdf)

        os.chdir(outputfolder)

        tmp_file = mkstemp(dir=outputfolder, suffix='.svg')
        document.write(tmp_file[1])

        # Convert to PDF + LaTeX.
        exec_command(['inkscape', '-z',  '-A',
                      self.outputpdf, '--export-latex',
                      tmp_file[1]])

        os.remove(tmp_file[1])
        
        with open(self.outputpdf.replace('.pdf','.pdf_tex'),'r') as f:
          pdf_tex = f.read()
          pdf_tex = pdf_tex.replace('\\makebox(0,0)[lb]{\\smash','\\makebox(0,0)[lt]{')
        with open(self.outputpdf.replace('.pdf','.pdf_tex'),'w') as f:
          f.write(pdf_tex)

        os.chdir(cwd)

if __name__ == '__main__':
    effect = TexTextExporter()
    effect.effect()
